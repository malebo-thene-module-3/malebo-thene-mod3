import 'package:flutter/material.dart';

void main(){
  runApp(MyApp());
}
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BaseApp(),
      routes: <String,WidgetBuilder> {
        '/LoginAsUser':(context) => LoginAsUser(),
        '/LoginAsGuest':(context) => LoginAsGuest(),
        '/BlankPage1':(context) => BlankPage1(),
        '/BlankPage2':(context) => BlankPage2(),
        '/EditProfile':((context) => EditProfile())
      },
    );
  }
}
class BaseApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("MTN...ACADEMY...APP"),),
      body: Center(
        child:Column(
          children:<Widget> [
            ElevatedButton(
              child: Text ("Login") ,
             // Theme:Colors.green,
              onPressed: (){
                Navigator.pushNamed(context, '/LoginAsUser');
              },
              ),

              ElevatedButton(
                child:Text('Register'),
               // Color: Colors.red,
                onPressed:() {
                  Navigator.pushNamed(context, '/LoginAsGuest');
                },
              )
          ],
          ),
           ),
      
    );
  }
}
class LoginAsUser extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Welcome User...'),),
      body: Center(
        child: Column(
          children: <Widget>[
            ElevatedButton(
              child: Text('BlankPage1'),
              onPressed:() {
                Navigator.pushNamed(context, '/BlankPage1');
              },
            ),
            ElevatedButton(
              child: Text('BlankPage2'),
              onPressed:() {
                Navigator.pushNamed(context, '/BlankPage2');
              },
            ),
            ElevatedButton(
              child: Text('Edit Profile'),
              onPressed: () {
                Navigator.pushNamed(context, '/LoginAsUser');
              }
            )
          ],
        ),
      )
    );
  }
}
    class LoginAsGuest extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Registration'),),
    );
  }
}
class BlankPage1 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('BlankPage1'),),
    );
  }
}
class BlankPage2 extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('BlankPage2'),),
    );
  }
}
class EditProfile extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Edit Your Profile'),),
    );
  }
}



